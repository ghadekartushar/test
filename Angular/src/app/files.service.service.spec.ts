import { TestBed } from '@angular/core/testing';

import { Files.ServiceService } from './files.service.service';

describe('Files.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Files.ServiceService = TestBed.get(Files.ServiceService);
    expect(service).toBeTruthy();
  });
});
